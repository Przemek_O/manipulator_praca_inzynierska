///Przemyslaw Olszowka/xx.xx.2020 -- sprawdzic czy chodzi :DD
#include "Arduino.h"

//---------Przypisanie PINow

//--------- PINY OD SYGNALOW PMW NA SILNIKI ------------------
#define J1_PWM 4
#define J2_PWM 5
#define J3_PWM 6
#define J4_PWM 3
#define J5_PWM 7

//---------- PINY OD KIERUNKOW OBROTU SILNIKA ----------------
#define J1_DIR 22
#define J2_DIR 24
#define J3_DIR 26
#define J5_DIR 28
#define J4_DIR 32

//---------- PINY OD POTENCJOMETROW -----------
#define J1_angel 0
#define J2_angel 1
#define J3_angel 2
#define J4_angel 3
#define J5_angel 8

//---------- PINY OD POMIARU PRADU -----------
#define J1_current 7
#define J2_current 6
#define J3_current 5
#define J4_current 9
#define J5_current 4

//---------- PINY OD KRANCOWEK ----------------

#define J3_lim_swi_max 10
#define J3_lim_swi_min 2
#define J4_lim_swi_max 11
#define J4_lim_swi_min 12

//--------- DODATKOWE

#define RedLed 52
#define GreenLed 50
#define ButtonPin 36

//____________________________________________________
//--------------Zakresy podawane przez port szeregowy
int Range1Min=270; // 0
int Range1Max=-270; // 360
int Range2Min=-86;
int Range2Max=6;
int Range3Min=5;
int Range3Max=-148;
int Range4Min=-100; //0
int Range4Max=100; //360
int Range5Min=-360;
int Range5Max=360;

//---------------------------- Parametry poszegolnych osi ----------------------------------------------------------------------
//----------os1------------------
int ZakMinRzecz1=0;   // minimalna - RZECZYWISTA -  wartosc odczytana z potenjometru na pierwszej osi (z zakresu 0-1024) 
int ZakMaxRzecz1=1024;   // maksymalna - RZECZYWISTA -  wartosc odczytana z potenjometru na pierwszej osi (z zakresu 0-1024)
int d_p1=3;             // dokładnosc pozycjonowania - wartośc wyrażona w "stopniach"(0-360)określająca zakres kątowy osiagnietego kata- "im mniej tym lepiej/dokladniej"
int s_w1=37;            // strefa "wolnej predkosci" - inormacja o kacie - wyznaczana eksperymentalnie, scisle powiazane z predkoscia maksymalna na danej osi , wyrażana w stopniach"0-360"
int ms1=230;             // maksymalna predkosc silnika na osi - liczba z zakresu od 0 do 255 - wypelnienie PWM
int PWMp1=50;           // minimalna predkosc silnika na osi - predkosc dojazdu do wynaczonych polazen w ostatniej fazie ruchu, minimalna wartosc przy ktorej silnik sie obraca - liczba z zakresu 0-255 - wypelnienie PWM
int J1_Speed_PWM= 60;   // predkosc ruchu przy uzywaniu bt
int J1_Speed_PWM_2=100;  
int PWMo1=0;            // zmienna stworzona na potrzeby funkcji//
bool k1=1;              // zmienna (0 lub 1) okreslająca kierunek ruchu do przodu dla danej osi(sprawdzana doświadczalnie)
int w_i1=4;             // zmienna okreslająca wartosci dodawanej lub odejmowanej wartosci podczas kolejnej iteracji przy rozpedzaniu lub hamowaniu silnika
//----------os2------------------
int ZakMinRzecz2=629; //749     //analogicznie do osi1
int ZakMaxRzecz2=749; //629       630,
int d_p2=5;
int s_w2=5;
int ms2=230;
int PWMp2=40;
int J2_Speed_PWM= 60;
int J2_Speed_PWM_2 = 100;
int PWMo2=0;
bool k2=0;
int w_i2=14;
//----------os3------------------
int ZakMinRzecz3=404;  //404    //analogicznie do osi1
int ZakMaxRzecz3=550;
int d_p3=5;
int s_w3=35;
int ms3=200;
int PWMp3=120;
int J3_Speed_PWM=100;
int J3_Speed_PWM_2=200;
int PWMo3=0;
bool k3=1;
int w_i3=10;

//----------os4------------------
int ZakMinRzecz4=169;      //analogicznie do osi1
int ZakMaxRzecz4=909;
int d_p4=2;
int s_w4=35;
int ms4=220;
int PWMp4=40;
int J4_Speed_PWM= 60;
int J4_Speed_PWM_2 = 120;
int PWMo4=0;
int k4=1; //0
int w_i4=8;
//----------os5--------------------
int ZakMinRzecz5=453;      //analogicznie do osi1
int ZakMaxRzecz5=958;
int d_p5=3;
int s_w5=35;
int ms5=150;
int PWMp5=20;
int J5_Speed_PWM= 40;
int J5_Speed_PWM_2=90;
int PWMo5=0;
int k5=1;
int w_i5=20;
double Axis5Val=0;

//------------ zmienne przechowujące wartość od - 100 do 100 informujace o kierunku i predkosci
int ch1 = 0;  
int ch2 = 0;
int ch3 = 0;
int ch4 = 0;
int ch5 = 0;
int ch6 = 0;

unsigned int Package=0;
double Axis1Val;
double Axis2Val;
double Axis3Val;
double Axis4Val;

int pulse_time;

//------ Tabela przechowująca przychodzaca wiadomosc z Bt
char characters[]={'-','-','-','-','-','-','-','-','-','-','-','-','-','-','-'};

//--- zmienne potrzebne do komunikacji Bt
bool flag = false;
bool flag_j = false;
bool flag_doit = true;
bool flag_led_gr = false;
int sum_j=0;
String dataIn = "";


//---- Odczyt zmiennych z POTENCJOMETROW
int POTEN1 = 0; //wartośc odczytywana bezpośrednio z potencjometru, domyslny zakres 0-1024
int POTEN2 = 0;
int POTEN3 = 0;
int POTEN4 = 0;
int POTEN5 = 0;
int POTEN11 = 0; //wartosc po zamianie impulsow na polozenie katowe w zadanym zakresie - wartosci podane w katach
int POTEN22 = 0;
int POTEN33 = 0;
int POTEN44 = 0;
int POTEN55 = 0;

//------- zmienne okreslajace stan przelacznikow i krancowek
bool J3_lim_swi_max_state = 0;
bool J3_lim_swi_min_state = 0;
bool J4_lim_swi_max_state = 0;
bool J4_lim_swi_min_state = 0;
bool ButtonState = 0;

//------- flagi do ruchu przy bt

int8_t j1_flag = 0;
int8_t j2_flag = 0;
int8_t j3_flag = 0;
int8_t j4_flag = 0;
int8_t j5_flag = 0;

//--------------------------------

// zmienne do każdej konfiguracji nowej czestotliwosci migania dioda led

unsigned long actual_time_1 = 0;
unsigned long actual_time_2 = 0;
unsigned long actual_time_3 = 0;
unsigned long bt_time = 0;
unsigned long time_last_message = 0;
unsigned long check_time_con_bt = 0;
int INTERVAL_MESSAGE = 100;
int interval_check_bt = 1000;


char temp = ' ';
int li = 0;
String temp1;


unsigned long ShowLed (unsigned long time__ ,int time1, int time2, int Pin){

    if ((millis()>time__) && (millis() < time__ + time1 ))
      {
        digitalWrite(Pin, true);
      }

    if ((millis()>time__ + time1) && (millis() < time__ + time1 + time2 ))
      {
        digitalWrite(Pin, false);
      }

    if (millis()>time__ + time1 + time2) 
      {
        time__ = millis();
      }

  return time__ ;
}


/*Wartosc zmiennej "Direct" uzaleznia kierunek obrotu silnika danej osi
*/

//------------------ CONTROL AXIS 1,2,3,4,5 ----------------------
int MOTOR_CONTROL(double SetAngel, int Potentiometer, int DIR, int PWM, bool Direct, double PrecisionSpeed, int ActualSpeed, int RangeMinReal,int RangeMaxReal, int RangeMin, int RangeMax, double PositioningAccuracy, double SlowSpeedZone, double MaxSpeed, int WIter, bool limit_switch_min_state, bool limit_switch_max_state)
{

  int ActualAngleValue=analogRead(Potentiometer);
  double RealAngel=map(ActualAngleValue,RangeMinReal,RangeMaxReal,RangeMin, RangeMax);
  double DiffAngelMark=SetAngel-RealAngel;
  double DiffAngel=abs(DiffAngelMark);

  if (DiffAngel>=PositioningAccuracy){
    if ((ActualSpeed<PrecisionSpeed)) { ActualSpeed=ActualSpeed+(WIter);} 
    if ((DiffAngel > SlowSpeedZone) && (ActualSpeed < MaxSpeed) && (ActualSpeed >= PrecisionSpeed)){ActualSpeed=ActualSpeed+WIter;}
    if ((DiffAngel<=SlowSpeedZone) && (DiffAngel>PositioningAccuracy) && (ActualSpeed>PrecisionSpeed) ){ActualSpeed=ActualSpeed-WIter;}
   }
  if (DiffAngel<PositioningAccuracy){
        if (ActualSpeed>0) {
          ActualSpeed=0;
          }
   }
 bool Turn1;
 bool flag_limit_switch;


  if(Direct)
          { 
              if (DiffAngelMark<0){
                Turn1 = 1;
                flag_limit_switch = limit_switch_max_state;
                }
              if (DiffAngelMark>0){
                Turn1=0; 
                flag_limit_switch = limit_switch_min_state;
                }
          }
  else
          {
              if (DiffAngelMark<0){
                Turn1=0;
                flag_limit_switch = limit_switch_min_state;
                }
              if (DiffAngelMark>0){
                Turn1=1;
                flag_limit_switch = limit_switch_max_state;
                } 
          }
//Serial.println(ActualSpeed);
if (flag_limit_switch == 1){
        analogWrite(PWM, ActualSpeed); 
        digitalWrite(DIR, Turn1);
      } 
    else 
      {
        analogWrite(PWM, 0); 
        digitalWrite(DIR, Turn1);
      }
  return ActualSpeed;
}

void MOTOR_CONTROL_BT(int DIR, int PWM, bool Direct, bool Front, double Speed_PWM){ 
// zmiena kierunek ruchu - opisująca czy w danej funkcji ruch ma sie odbywac w przód czy w tył - front
  bool Turn=0;

  if(Direct)
          { 
              if (Front){
                Turn=1;
                }
              else{
                Turn=0; 
                }
          }
  else
          {
              if (Front){
                Turn=0;
                }
              else{
                Turn=1;
                } 
          }
  analogWrite(PWM, Speed_PWM); 
  digitalWrite(DIR, Turn);
}



// ------------------ INICJALIZACJA ---------------------

void setup() {
Serial1.begin(9600); // KOMUNIKACJA GRIPPER
Serial3.begin(9600);  // KOMUNIKACJA BLUETOOTH
Serial.begin(9600);  // KOMUNIKACJA PC

pinMode(GreenLed, OUTPUT);
pinMode(RedLed, OUTPUT);
pinMode(ButtonPin, INPUT);
pinMode(J3_lim_swi_max, INPUT);
pinMode(J3_lim_swi_min, INPUT);
pinMode(J4_lim_swi_max, INPUT);
pinMode(J4_lim_swi_min, INPUT);

POTEN1 = analogRead (J1_angel);
POTEN2 = analogRead (J2_angel);
POTEN3 = analogRead (J3_angel);
POTEN4 = analogRead (J4_angel);
POTEN5 = analogRead (J5_angel);
POTEN11 = map(POTEN1,ZakMinRzecz1,ZakMaxRzecz1,Range1Max,Range1Min);
POTEN22 = map(POTEN2,ZakMinRzecz2,ZakMaxRzecz2,Range2Max,Range2Min);
POTEN33 = map(POTEN3,ZakMinRzecz3,ZakMaxRzecz3,Range3Max,Range3Min);
POTEN44 = map(POTEN4,ZakMinRzecz4,ZakMaxRzecz4,Range4Max,Range4Min);
POTEN55 = map(POTEN5,ZakMinRzecz5,ZakMaxRzecz5,Range5Max,Range5Min);



// ------------------ WARTOSCI POCZATKOWE - DEFINICJA POZYCJI DOMYSLNEJ MANIPA (domyslnie wartosc z zakresu 0-360)-----------------------
Axis1Val = POTEN11; //wartoscie katowe od o do 360
Axis2Val = POTEN22;
Axis3Val = POTEN33;
Axis4Val = POTEN44;
Axis5Val = POTEN55;

actual_time_1 = 0;
actual_time_2 = 0;
actual_time_3 = 0;
bt_time = 0;
check_time_con_bt = 0;

Serial3.setTimeout(25);
Serial3.print('r');

}
//--------------------- PETLA LOOP - PROGRAM -----------------------------------------
void loop() {


POTEN1 = analogRead (J1_angel);
POTEN2 = analogRead (J2_angel);
POTEN3 = analogRead (J3_angel);
POTEN4 = analogRead (J4_angel);
POTEN5 = analogRead (J5_angel);
POTEN11 = map(POTEN1,ZakMinRzecz1,ZakMaxRzecz1,Range1Min,Range1Max);
POTEN22 = map(POTEN2,ZakMinRzecz2,ZakMaxRzecz2,Range2Min,Range2Max);
POTEN33 = map(POTEN3,ZakMinRzecz3,ZakMaxRzecz3,Range3Min,Range3Max);
POTEN44 = map(POTEN4,ZakMinRzecz4,ZakMaxRzecz4,Range4Min,Range4Max);
POTEN55 = map(POTEN5,ZakMinRzecz5,ZakMaxRzecz5,Range5Min,Range5Max);  

//--------------ODCZYT WARTOSCI KATOWYCH Z KONSOLI KOMPUTERA ----------------
            //-----------DEKODOWANIE DANYCH Z KONSOLI-------------  
  
ButtonState = digitalRead(ButtonPin);
J3_lim_swi_max_state = digitalRead(J3_lim_swi_max);
J3_lim_swi_min_state = digitalRead(J3_lim_swi_min);
J4_lim_swi_max_state = digitalRead(J4_lim_swi_max);
J4_lim_swi_min_state = digitalRead(J4_lim_swi_min);

/*
Serial.print(J3_lim_swi_max_state);
Serial.print("\t");
Serial.print(J3_lim_swi_min_state);
Serial.print("\t");
Serial.print(J4_lim_swi_max_state);
Serial.print("\t");
Serial.print(J4_lim_swi_min_state);
Serial.print("\t");
Serial.println(ButtonState);
*/

if(ButtonState){  
    actual_time_2 = ShowLed(actual_time_2,200,200,RedLed);
  }
else
  {
    actual_time_3 = ShowLed(actual_time_3,100,900,RedLed);
  }
    
// start Bt

  if (Serial3.available()) {

    flag_led_gr=true;
    check_time_con_bt = millis();

    temp1 = Serial3.readString();
    temp1.toCharArray(characters,23);

    for(int k=0; k<15; ++k){
      Serial.print(characters[k]);
      Serial.print("\t");
    }
    Serial.println();

    if (characters[0]=='r') {
      flag=true;
      }
    else {
        Serial3.println("Error: NO MASSAGE");
        Serial.println("Error: NO MASSAGE");
      }

    if (characters[14]!='X') {
      Serial3.println("Error: incomplete message X ");
      Serial.println("Error: incomplete message X ");
    }

    Serial3.println(characters);
    
        if (flag == true)
        {
          if(millis() > time_last_message + INTERVAL_MESSAGE) 
          {
              time_last_message = millis();
              Serial.println("srodek");
              Serial3.println("<r>");
        
              Serial3.print("<v1>");
              Serial3.print(POTEN11);
              Serial3.println("</v1>");

              Serial3.print("<v2>");
              Serial3.print(POTEN22);
              Serial3.println("</v2>");

              Serial3.print("<v3>");
              Serial3.print(POTEN33);
              Serial3.println("</v3>");

              Serial3.print("<v4>");
              Serial3.print(POTEN44);
              Serial3.println("</v4>");

              Serial3.print("<v5>");
              Serial3.print(POTEN55);
              Serial3.println("</v5>");

              Serial3.print("<k1>");
              Serial3.print(J3_lim_swi_max_state);
              Serial3.println("</k1>");

              Serial3.print("<k2>");
              Serial3.print(J3_lim_swi_min_state);
              Serial3.println("</k2>");

              Serial3.print("<k3>");
              Serial3.print(J4_lim_swi_max_state);
              Serial3.println("</k3>");

              Serial3.print("<k4>");
              Serial3.print(J4_lim_swi_min_state);
              Serial3.println("</k4>");

              Serial3.println("</r>");
          }          
        }    
  }

flag = true;
  
// koniec Bt

if(millis() > check_time_con_bt + interval_check_bt) { 
//jeżeli nastapi wejscie do tej petli, oznacza to ze przez 1 sek nie bylo zadnej informacji z bt;
    check_time_con_bt = millis();

    digitalWrite(GreenLed,false);
    flag_led_gr=false;
}

//odczytywanie informacji z portu szeregowego USB

if ( Serial.available() > 0 ) {
    String PackageString=Serial.readStringUntil('\n');
    Package=PackageString.toInt();
    Serial.print(Package);
    Serial.println("\t");

//-------PRZYPISANIE WARTOSCI -------------------   

    if ((Package>=1000) && (Package<=1360)){
      Axis1Val=Package-1000;
      Axis1Val = map(Axis1Val, 0, 360, Range1Min, Range1Max);
    }

    if ((Package>=2000) && (Package<=2360)){
      Axis2Val=Package-2000;
      Axis2Val = map(Axis2Val, 0, 360, Range2Min, Range2Max);
    }

    if ((Package>=3000) && (Package<=3360)){
      Axis3Val=Package-3000;
      Axis3Val = map(Axis3Val, 0, 360, Range3Min, Range3Max);
    }

    if ((Package>=4000) && (Package<=4360)){
      Axis4Val=Package-4000;
      Axis4Val = map(Axis4Val, 0, 360, Range4Min, Range4Max);
    }

    if ((Package>=5000) && (Package<=5360)){
      Axis5Val=Package-5000;
      Axis5Val = map(Axis5Val, 0, 360, Range5Min, Range5Max);
    }

char msg[5] = "----"; //String data

String voice = msg;
int znak = 0;

}


if (flag_led_gr != true) {
  // Wykonywanie ruchu przy pomocy komend wysylanych z PC
  Serial.print("PC MODE "); 
  digitalWrite(GreenLed,false);
  PWMo1=MOTOR_CONTROL( Axis1Val , J1_angel , J1_DIR , J1_PWM , k1 , PWMp1 , PWMo1 , ZakMinRzecz1 , ZakMaxRzecz1 , Range1Max , Range1Min , d_p1 , s_w1 , ms1 , w_i1 , true , true );
  PWMo2=MOTOR_CONTROL( Axis2Val , J2_angel , J2_DIR , J2_PWM , k2 , PWMp2 , PWMo2 , ZakMinRzecz2 , ZakMaxRzecz2 , Range2Max , Range2Min , d_p2 , s_w2 , ms2 , w_i2 , true , true );
  PWMo3=MOTOR_CONTROL( Axis3Val , J3_angel , J3_DIR , J3_PWM , k3 , PWMp3 , PWMo3 , ZakMinRzecz3 , ZakMaxRzecz3 , Range3Max , Range3Min , d_p3 , s_w3 , ms3 , w_i3 , J3_lim_swi_min_state , J3_lim_swi_max_state );
  PWMo4=MOTOR_CONTROL( Axis4Val , J4_angel , J4_DIR , J4_PWM , k4 , PWMp4 , PWMo4 , ZakMinRzecz4 , ZakMaxRzecz4 , Range4Max , Range4Min , d_p4 , s_w4 , ms4 , w_i4 , J4_lim_swi_min_state , J4_lim_swi_max_state );
  PWMo5=MOTOR_CONTROL( Axis5Val , J5_angel , J5_DIR , J5_PWM , k5 , PWMp5 , PWMo5 , ZakMinRzecz5 , ZakMaxRzecz5 , Range5Max , Range5Min , d_p5 , s_w5 , ms5 , w_i5 , true , true );
  }   
else{

//Wykonywanie ruchu przy pomocy komend wysyłanych z BT

      Serial.println("BT Mode");
      actual_time_1 = ShowLed(actual_time_1,300,300,GreenLed);

if((characters[2] == '1') && (POTEN11 < Range1Min)){
  MOTOR_CONTROL_BT(J1_DIR, J1_PWM, k1, true, J1_Speed_PWM);
  }
else if((characters[3] == '1') && (POTEN11 > Range1Max)){
  MOTOR_CONTROL_BT(J1_DIR, J1_PWM, k1, false, J1_Speed_PWM);
  }
else if ((characters[2] == '2') && (POTEN11 < Range1Min)){
  MOTOR_CONTROL_BT(J1_DIR, J1_PWM, k1, true, J1_Speed_PWM_2);
    }
else if ((characters[3] == '2') && (POTEN11 > Range1Max)){
  MOTOR_CONTROL_BT(J1_DIR, J1_PWM, k1, false, J1_Speed_PWM_2);
  }
else {
    MOTOR_CONTROL_BT(J1_DIR, J1_PWM, k1, false, 0);
  }

if((characters[4] == '1')  && (POTEN22 < Range2Max)){
  MOTOR_CONTROL_BT(J2_DIR, J2_PWM, k2, true, J2_Speed_PWM);
  }
else if((characters[5] == '1')  && (POTEN22 > Range2Min)){
  MOTOR_CONTROL_BT(J2_DIR, J2_PWM, k2, false, J2_Speed_PWM);
  }
else if ((characters[4] == '2')  && (POTEN22 < Range2Max)){
  MOTOR_CONTROL_BT(J2_DIR, J2_PWM, k2, true, J2_Speed_PWM_2);
  }
else if((characters[5] == '2')  && (POTEN22 > Range2Min)){
  MOTOR_CONTROL_BT(J2_DIR, J2_PWM, k2, false, J2_Speed_PWM_2);
  }
else {
    MOTOR_CONTROL_BT(J2_DIR, J2_PWM, k2, false, 0);
  }

if((characters[6] == '1') && (POTEN33 < Range3Min) && (J3_lim_swi_max_state == 1)){
  MOTOR_CONTROL_BT(J3_DIR, J3_PWM, k3, true, J3_Speed_PWM);
  }
else if((characters[7] == '1') && (POTEN33 > Range3Max) && (J3_lim_swi_min_state == 1)){
  MOTOR_CONTROL_BT(J3_DIR, J3_PWM, k3, false, J3_Speed_PWM);
  }
else if((characters[6] == '2') && (POTEN33 < Range3Min) && (J3_lim_swi_max_state == 1)){
  MOTOR_CONTROL_BT(J3_DIR, J3_PWM, k3, true, J3_Speed_PWM_2);
  }
else if((characters[7] == '2') && (POTEN33 > Range3Max) && (J3_lim_swi_min_state == 1)){
  MOTOR_CONTROL_BT(J3_DIR, J3_PWM, k3, false, J3_Speed_PWM_2);
  }
else {
    MOTOR_CONTROL_BT(J3_DIR, J3_PWM, k3, false, 0);
  }

if((characters[8] == '1') && (POTEN44 < Range4Max) && (J4_lim_swi_max_state == 1)){
  MOTOR_CONTROL_BT(J4_DIR, J4_PWM, k4, true, J4_Speed_PWM);
  }
else if((characters[9] == '1') && (POTEN44 > Range4Min) && (J4_lim_swi_min_state == 1)){
  MOTOR_CONTROL_BT(J4_DIR, J4_PWM, k4, false, J4_Speed_PWM);
  }
else if((characters[8] == '2') && (POTEN44 < Range4Max) && (J4_lim_swi_max_state == 1)){
  MOTOR_CONTROL_BT(J4_DIR, J4_PWM, k4, true, J4_Speed_PWM_2);
  }
else if((characters[9] == '2') && (POTEN44 > Range4Min) && (J4_lim_swi_min_state == 1)){
  MOTOR_CONTROL_BT(J4_DIR, J4_PWM, k4, false, J4_Speed_PWM_2);
  }
else {
    MOTOR_CONTROL_BT(J4_DIR, J4_PWM, k4, false, 0);
  }

if((characters[10] == '1') && (POTEN55 < Range5Max)){
  MOTOR_CONTROL_BT(J5_DIR, J5_PWM, k5, true, J5_Speed_PWM);
  }
else if((characters[11] == '1') && (POTEN55 > Range5Min)){
  MOTOR_CONTROL_BT(J5_DIR, J5_PWM, k5, false, J5_Speed_PWM);
  }
else if((characters[10] == '2') && (POTEN55 < Range5Max)){
  MOTOR_CONTROL_BT(J5_DIR, J5_PWM, k5, true, J5_Speed_PWM_2);
  }
else if((characters[11] == '2') && (POTEN55 > Range5Min)){
  MOTOR_CONTROL_BT(J5_DIR, J5_PWM, k5, false, J5_Speed_PWM_2);
  }
else {
    MOTOR_CONTROL_BT(J5_DIR, J5_PWM, k5, false, 0);
  }

//Wysyłanie komunikatów do Grippera

switch (characters[12]){
  case '0':
    
    break;
  case '1':
      Serial1.print('1');
      Serial.print("Gripper state: 1");
    break;
  case '2':
      Serial1.print('2');
      Serial.print("Gripper state: 2");
    break;
  case '3':
      Serial1.print('3');
      Serial.print("Gripper state: 3");
    break;
  case '4':
      Serial1.print('4');
      Serial.print("Gripper state: 4");
    break;

}

POTEN1 = analogRead (J1_angel);
POTEN2 = analogRead (J2_angel);
POTEN3 = analogRead (J3_angel);
POTEN4 = analogRead (J4_angel);
POTEN5 = analogRead (J5_angel);
POTEN11 = map(POTEN1,ZakMinRzecz1,ZakMaxRzecz1,Range1Max,Range1Min);
POTEN22 = map(POTEN2,ZakMinRzecz2,ZakMaxRzecz2,Range2Max,Range2Min);
POTEN33 = map(POTEN3,ZakMinRzecz3,ZakMaxRzecz3,Range3Max,Range3Min);
POTEN44 = map(POTEN4,ZakMinRzecz4,ZakMaxRzecz4,Range4Max,Range4Min);
POTEN55 = map(POTEN5,ZakMinRzecz5,ZakMaxRzecz5,Range5Max,Range5Min);

// ------------------ WARTOSCI POCZATKOWE - DEFINICJA POZYCJI DOMYSLNEJ MANIPA (domyslnie wartosc z zakresu 0-360)-----------------------
Axis1Val = POTEN11; //wartoscie katowe od o do 360
Axis2Val = POTEN22;
Axis3Val = POTEN33;
Axis4Val = POTEN44;
Axis5Val = POTEN55;
}
      

Serial.print(POTEN1);
Serial.print(" ");
Serial.print(POTEN2);
Serial.print(" ");
Serial.print(POTEN3);
Serial.print(" ");
Serial.print(POTEN4);
Serial.print(" ");
Serial.print(POTEN5);
Serial.print("|");
Serial.print(POTEN11);
Serial.print(" ");
Serial.print(POTEN22);
Serial.print(" ");
Serial.print(POTEN33);
Serial.print(" ");
Serial.print(POTEN44);
Serial.print(" ");
Serial.print(POTEN55);
Serial.println("\t");
Serial.println("\t");

delay(1);
}
